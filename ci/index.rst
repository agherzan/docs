.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Continuous Integration
======================

|main_project_name| ``git`` repositories hosted on https://booting.oniroproject.org/distro use
GitLab pipelines for building and testing changes before they are merged.
Individual pipelines are documented in each repository, but some general or more
important elements are described below.

.. toctree::
   :maxdepth: 1

   oniro
   docs
   device-testing
