.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: definitions.rst

Building |main_project_name| Documentation
##########################################

This topic outlines the standard way of generating the |main_project_name|
project documentation locally using the source files available in the
`booting.oniroproject.org <https://booting.oniroproject.org/distro/docs>`__ repository which
aggregates documentation from multiple other components.

Overview
********

The |main_project_name| documentation is written in reStructuredText markup
language (``.rst`` file extension) with Sphinx extensions to generate a
structured stand-alone website.

Prerequisites
*************
To generate the HTML documentation locally, you need to have the following 
packages pre-installed in your system:

* Sphinx (for more details on Sphinx installation, `check Sphinx Getting Started documentation <https://www.sphinx-doc.org/en/master/usage/quickstart.html>`_)
* The following Sphinx Extensions

   * sphinx-tabs (supports tabbed content in the documentation) 
   * sphinxcontrib.plantuml (supports plantuml content) 

* Plantuml (supports UML diagrams)

  The method of installing the `plantuml` package is dependent on your Linux 
  distribution.  For example, on a Ubuntu host, you can install `plantuml` 
  using the official package repository:
    
  .. code-block:: console
	
     $ sudo apt-get update -y
     $ sudo apt-get install -y plantuml

* Make (to build the documentation using the provided Makefile)

Building the Documentation
**************************

To generate a local copy of |main_project_name| documentation, perform the
following steps:

1. Create a local workspace and clone the |main_project_name| project files to
   your local, refer to :ref:`setting up a repo workspace <RepoWorkspace>`
   section for more information.
2. To generate output as HTML, run the following command:

.. code-block:: console

   $ make

The HTML output is built and can be viewed in your browser from the *<docs
repository>/build/index.html* path.

.. note::

   * All the local Sphinx warnings and errors generated during the build
     process must be fixed and validated.
   * To validate the changes, execute ``make clean && make`` command to
     generate a clean HTML output.

Reference
*********

`<https://www.sphinx-doc.org/en/master/contents.html>`_

`<https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_
